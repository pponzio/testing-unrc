package testing.evosuite.ejercicio1;

public class Stack<T> {
    
    private int total;
    private Node<T> first;
    
    public Stack() { }

    public void push(T ele)
    {
        Node<T> current = first;
        first = new Node<T>();
        first.setEle(ele);
        first.setNext(current);
    }

    public Object pop()
    {
        if (first == null) 
        		throw new IllegalArgumentException();

        Object ele = first.getEle();
        first = first.getNext();
        total--;
        return ele;
    }
    
    public boolean isEmpty(){
    		return total == 0;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        Node<T> tmp = first;
        while (tmp != null) {
            sb.append(tmp.getEle()).append(", ");
            tmp = tmp.getNext();
        }
        return sb.toString();
    }
    
}