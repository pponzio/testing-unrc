package testing.evosuite.teoria;

public class Node {
	int value;

	Node next;

	public String toString() {
		return "[" + value + "]";
	}
}