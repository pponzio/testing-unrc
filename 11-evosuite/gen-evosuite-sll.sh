#!/bin/bash

# Correr evosuite por 60 segundos en SinglyLinkedList con todas las fitness activadas (branch, mutation, etc...)
java -jar libs/evosuite-1.0.6.jar -class testing.evosuite.teoria.SinglyLinkedList -projectCP build/classes/java/main/ -Dsearch_budget=60

# Notas: 
#------
# - Agregar -criterion branch para generar solo tests para lograr branch coverage
# - Los tests generados quedan en la carpeta evosuite-tests. Puede moverlos a src/test/java para correrlos desde eclipse.
# - Por defecto, los tests de evosuite dan 0 de cobertura en jacoco. Para poder medir cobertura con jacoco, setear el parámetro separateClassLoader = false en SinglyLinkedList_ESTest.java y luego correr los tests de esa clase y medir cobertura.
