#!/bin/bash

java -cp libs/randoop-all-4.2.4.jar:build/classes/java/main/ randoop.main.Main gentests --testclass=testing.randoop.teoria.SinglyLinkedList --time-limit=10 --junit-output-dir=src/test/java/ --junit-package-name=testing.randoop.teoria --regression-test-basename=RandoopSLLTests


