package testing.randoop.ejercicio2;


import java.util.Collection;

import java.util.Iterator;
import java.util.LinkedList;
import randoop.CheckRep;

public class SetOverSortedList {

	//elems almacena de forma ordenada (por el hashcode) los elementos del conjunto.
	private LinkedList<Object> elems;
	
	//size representa la cantidad de elementos en el conjunto.
	private int size;
	
	public SetOverSortedList() {
		elems = new LinkedList<Object>();
		size = 0;
	}
	
	public SetOverSortedList(Collection<Object> s) {
		Iterator<Object> it = s.iterator();
		this.elems = new LinkedList();
		this.size = 0;
		while (it.hasNext()){
			this.add(it.next());
		}
	}
	
	public int size() {
		return size;
	}
	
	public boolean isEmpty() {
		return size() == 0;
	}

	//Intersección de conjuntos.
	public SetOverSortedList intersect(SetOverSortedList s) {
		if(s==null)
			throw new IllegalArgumentException();
		
		SetOverSortedList res = new SetOverSortedList();
	    for (Object e : elems) {
	        if (s.belongs(e)) {
	            res.add(e);
	        }
	    }
	    return res;
	}
	
	//add agrega el elemento e al conjunto.
	//retorna falso si elemento ya existía.
	//en caso contrario, e es insertado en forma ordenado, respecto de su hashcode.
	public boolean add(Object e) {
		if (belongs(e))
			return false;

		int i = 0;
		while(i<size()){
			if(elems.get(i).hashCode()>=e.hashCode()){
				break;
			}
			i++;
		}
		elems.add(i, e);
		size++;
		
		return true;
	}
	
	

	//Union de conjuntos.
	public SetOverSortedList union(SetOverSortedList s) {
		if(s==null)
			throw new IllegalArgumentException();
		
		SetOverSortedList res = new SetOverSortedList();
        for (Object e : this.elems)
            res.add(e);
        
        for (Object e : s.elems)
            res.add(e);
        
        return res;
	}

		
	//contains retorna true, sii, o pertenece a elems.
	public boolean belongs(Object o) {
		for (int i=1; i< size(); i++){
			if(elems.get(i).equals(o)){
				return true;
			}
		}
		return false;
	}

	


	//remueve el elemento o del conjunto.
	//retorna false si o no pernetecía al conjunto.
	//retorna true si efectivamente el elemento fue eliminado.
	public boolean remove(Object o) {
		for (int i=0; i< size(); i++){
			if(elems.get(i).equals(o)){
				elems.remove(i);
				size--;
				return true;
			}
		}
		return false;
	}

	
	//Eliminar todos los elementos del conjunto.
	public void clear() {
		for(Object e : elems)
			remove(e);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((elems == null) ? 0 : elems.hashCode());
		result = prime * result + size;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		SetOverSortedList other = (SetOverSortedList) obj;
		
		if (elems == null) {
			if (other.elems != null)
				return false;
		} else if (size != other.size)
			return false;
		else if (!elems.containsAll(other.elems))
			return false;
		return true;
	}

	@CheckRep
	public boolean repOK() {
		return false;
	}
	
	
}
