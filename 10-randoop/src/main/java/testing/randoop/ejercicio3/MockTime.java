package testing.randoop.ejercicio3;

import java.io.Serializable;

public class MockTime implements ITime, Serializable {

	public  static final long serialVersionUID = 1;
	private static long currTime;
	public long getCurrentTime() {
		currTime = currTime + 1000;
		return currTime;
	}

}
