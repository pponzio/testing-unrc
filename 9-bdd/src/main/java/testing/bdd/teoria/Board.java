package testing.bdd.teoria;

import java.util.List;

public class Board {
	
	private static final int TABLE_SIZE = 8; 

	// Characteristics: 
	// C1: Mine in first row (T,F), 
	// C2: Mine in last row (T,F), 
	// C3: Mine in first column (T,F), 
	// C4: Mine in last column (T,F)

//	public static Board loadBoardFromString(List<List<String>> table) {
//		return null;
//	}

	public static Board loadBoardFromStringTable(List<List<String>> table) {
		Board res = new Board();
		for (int row = 0; row < TABLE_SIZE; row++) {
			List<String> currRow = table.get(row);
			for (int column = 0; column < TABLE_SIZE; column++) {
				String currElem = currRow.get(column);
				if (currElem == "*")
					res.putMine(row + 1, column + 1);
			}
		}
		return res;
	}

	private void putMine(int i, int j) {
		
	}

	public boolean hasMine(int i, int j) {
		return false;
	}

	public int getTotalMines() {
		return -1;
	}

}
