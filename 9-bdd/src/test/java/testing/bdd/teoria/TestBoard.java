package testing.bdd.teoria;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

public class TestBoard {
	
	/*
	 * Characteristics for testing loadBoardFromStringTable: 
	 *     C1: Mine in first row (T,F), 
	 *     C2: Mine in last row (T,F), 
	 *     C3: Mine in first column (T,F), 
	 *     C4: Mine in last column (T,F)
	 *     Coverage criteria: Pairwise
	 */
	// Test for case TTTT 
	@Test
	public void loadBoardWithMinesInLimits() {
		List<List<String>> table = new ArrayList<>();
		table.add(Arrays.asList("*", "", "", "", "", "", "", ""));
		table.add(Arrays.asList("", "", "", "", "", "", "", ""));
		table.add(Arrays.asList("", "", "", "", "", "", "", ""));
		table.add(Arrays.asList("", "", "", "", "", "", "", ""));
		table.add(Arrays.asList("", "", "", "", "", "", "", ""));
		table.add(Arrays.asList("", "", "", "", "", "", "", ""));
		table.add(Arrays.asList("", "", "", "", "", "", "", ""));
		table.add(Arrays.asList("", "", "", "", "", "", "", "*"));
		Board board = Board.loadBoardFromStringTable(table);
		assertTrue(board.hasMine(1,1));
		assertTrue(board.hasMine(8,8));
		assertEquals(2, board.getTotalMines());
	}
	
	/*
	 *  TODO: Completar testing TDD de loadBoardFromStringTable
	 */
	
	/* 
	 * Characteristics for testing putMine
	 *   M: Mines on board
	 *     M1: zero
	 *     M2: one
	 *     M3: more than one
	 *
	 *   P: Board position 
	 *     P1: Empty 
	 *     P2: Mine
	 *     P3: Invalid
	 * Coverage criteria: Pairwise
	 */
	
	// Pair (M1,P1)
	@Test
	public void putAMineInEmptyPosition() {
		Game g = new Game();

		assertTrue(g.putMine(2,3));
	}
	
	// Pair (M2,P2)
	@Test
	public void putAMineOnAMine() {
		Game g = new Game();
		assertTrue(g.putMine(2,3));

		assertFalse(g.putMine(2,3));
	}
	
	/*
	 *  TODO: Completar testing TDD de putMine 
	*/

}
