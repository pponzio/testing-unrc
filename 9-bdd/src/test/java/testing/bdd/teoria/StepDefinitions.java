package testing.bdd.teoria;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class StepDefinitions {

	Board board;
	Game game;
	
    @Given("the desired board state is")
    public void the_desired_board_state_is(DataTable dataTable) {
        List<List<String>> table = dataTable.asLists(String.class);
    	board = Board.loadBoardFromStringTable(table);
    }

    @When("the game is created")
    public void the_game_is_created() {
    	game = new Game();
    	game.setBoard(board);
    }
    
    @Then("there is a mine on position {int},{int}")
    public void there_is_a_mine_on_position(int row, int col) {
    	assertTrue(game.hasMine(row,col));
    }

    @Then("there are a total of {int} mines")
    public void there_are_a_total_of_mines(int numMines) {
    	assertEquals(numMines, game.getNumberMines());
    }

}
