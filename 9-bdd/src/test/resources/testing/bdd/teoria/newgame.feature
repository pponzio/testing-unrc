# Characteristic: Game difficulty
#	B1: Easy (<=3 mines)
#   B2: Medium (>3 and <=9 mines)
#   B3: Hard (>9 mines)
Feature: Create a new game

  Scenario: Create a new game on easy mode
    Given the desired board state is
		|*|	| | | | | | |
		| |	| | | | | | |
		| |	| | |*| | | |
		| |	| | | | | | |
		| |	| | | | | | |
		| |	| | | | | | |
		| |	| | | |*| | |
		| |	| | | | | | |
    When the game is created
    Then there is a mine in position 1,1
    And there is a mine in position 3,5
    And there is a mine in position 7,6
    And there are a total of 3 mines
    

  Scenario: Create a new game on normal mode
    # TODO: Define this scenario...
    
  Scenario: Create a new game on hard mode
    # TODO: Define this scenario...
    