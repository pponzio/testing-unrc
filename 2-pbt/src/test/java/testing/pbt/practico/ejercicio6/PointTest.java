package testing.pbt.practico.ejercicio6;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

//Introduction to Software Testing
//Authors: Paul Ammann & Jeff Offutt

public class PointTest {
	   private Point p  = new Point(1,2);
	   private ColorPoint cp1   = new ColorPoint(1,2,Color.RED);
	   private ColorPoint cp2   = new ColorPoint(1,2,Color.BLUE);

	   // this test fails!
	   @Test public void symmetry() {
		   System.out.println(p.equals(cp1));
		   System.out.println(cp1.equals(p));
	      assertEquals(p.equals(cp1), cp1.equals(p));
	   }

	   // this test passes
	   @Test public void transitivity() {
	      if (cp1.equals(p) && p.equals(cp2)) {
	         assertTrue(cp1.equals(cp2));
	      }
	   }
	   
	}
