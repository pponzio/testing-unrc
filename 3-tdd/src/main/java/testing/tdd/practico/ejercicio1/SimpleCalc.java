package testing.tdd.practico.ejercicio1;

import java.util.Arrays;
import java.util.List;

public class SimpleCalc {
	
	static List<String> delimiters;
	//= Arrays.asList(",", "\n");
	
	public static boolean containsDelimiter(String s) {
		for (String delim: delimiters) {
			if (s.contains(delim))
				return true;
		}
		return false;
	}
	
	// Pre: s contains a delimiter
	public static int indexOfFirstDelimiter(String s) {
		int res = s.length();
		for (String delim: delimiters) {
			int delimIndex = s.indexOf(delim);
			if (delimIndex != -1)
				res = Math.min(res, delimIndex);
		}
		
		if (res == s.length())
			throw new IllegalArgumentException(
					"Precondition violation. " + s + 
					" does not contain a delimiter (" + 
							delimiters.toString() + ")");
		
		return res;
	}

	public static int add(String numbers) {
		delimiters = Arrays.asList(",", "\n");
		
		// Caso ""
		if (numbers.isEmpty())
			return 0;
		
		// Caso //[delim]\n[numbers]...
		if (numbers.startsWith("//")) {
			String newDelim = numbers.substring(2, 3);
			delimiters = Arrays.asList(newDelim);
			numbers = numbers.substring(4);
		}
		
		// Caso un elemento
		if (!containsDelimiter(numbers)) 
			return Integer.parseInt(numbers);
		
		// Caso mas de un elemento
		String rest = numbers;
		int accum = 0;
		do {
			int delimIndex = indexOfFirstDelimiter(rest);
			String fst = rest.substring(0, delimIndex);
			accum += Integer.parseInt(fst); 
			rest = rest.substring(delimIndex+1);
		}
		while (containsDelimiter(rest));
		accum += Integer.parseInt(rest);

		return accum;
	}
	
}
